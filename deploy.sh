#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns

# La commande ci-dessous cr'ee un r'epertoire /etc/named sur la machine dwikiorg
himage dwikiorg mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans wiki.org/named.conf sur la machine dwikiorg dans le r'epertoire /etc/
hcp wiki.org/named.conf dwikiorg:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans wiki.org/* dans le r'epertoire  /etc/named sur la machine dwikiorg
hcp wiki.org/* dwikiorg:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dwikiorg
himage dwikiorg rm /etc/named/named.conf 


## Configuration de iut.re 

himage diutre mkdir -p /etc/named
hcp iut.re/named.conf diutre:/etc/.
hcp iut.re/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 


## configuration de pc1
hcp pc1/resolv.conf pc1:/etc/.

## Configuration de pc2
hcp pc2/resolv.conf pc2:/etc/.

## Configuration de .org
himage dorg mkdir -p /etc/named
hcp .org/named.conf dorg:/etc/.
hcp .org/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf

## Configuration de rt.iut.re
himage drtiutre mkdir -p /etc/named
hcp rt.iut.re/named.conf drtiutre:/etc/.
hcp rt.iut.re/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf

## Configuration de .re
himage dre mkdir -p /etc/named
hcp .re/named.conf dre:/etc/.
hcp .re/* dre:/etc/named/.
himage dre rm /etc/named/named.conf

## Configuration de . (serveur racine)
himage aRootServer mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf
