## Lancement des serveurs web

echo -e "Lancement du serveur www2 (iut.re)"
himage www2 mkdir -p /tmp/www
hcp www/2/* www2:/tmp/www/.
himage www2 python -m SimpleHTTPServer 80

echo -e "Lancement du serveur www1 (rt.iut.re)"
himage www1 mkdir -p /tmp/www
hcp www/1/* www1:/tmp/www/.
himage www1 python -m SimpleHTTPServer 80

echo -e "Lancement du serveur www3 (wiki.org)"
himage www3 mkdir -p /tmp/www
hcp www/3/* www3:/tmp/www/.
himage www3 python -m SimpleHTTPServer 80
