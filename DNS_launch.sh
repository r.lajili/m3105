## Lancement des serveurs DNS

# Lancement de dwikiorg
echo -e "Lancement de dwikiorg (wiki.org)"
himage dwikiorg killall -9 named
himage dwikiorg named -c /etc/named.conf

# Lancement de diutre
echo -e "Lancement de diutre (iut.re)"
himage diutre killall -9 named
himage diutre named -c /etc/named.conf

# Lancement de drtiutre
echo -e "Lancement de drtiutre (rt.iut.re)"
himage drtiutre killall -9 named
himage drtiutre named -c /etc/named.conf

# Lancement de dre
echo -e "Lancement de dre (.re)"
himage dre killall -9 named
himage dre named -c /etc/named.conf

# Lancement de dorg
echo -e "Lancement de dorg (.org)"
himage dorg killall -9 named
himage dorg named -c /etc/named.conf

# Lancement de aRootServer
echo -e "Lancement de aRootServer (.)"
himage aRootServer killall -9 named
himage aRootServer named -c /etc/named.conf

