Instructions concernant le lancement des différents serveurs DNS

Premièrement il vous faut récupérer tous les fichiers de configurations présents dans les divers dossiers (.org , .re ...)

Ensuite posseder la machine IMUNES-11.0-RELEASE disponible sur le cloud R&T

Placer tous les fichiers dans le repertoire /root/imunes/configs

Lancer IMUNES et ouvrir le fichier /root/mydns2.imn (récupérable au préalable, les explications sont fournis dans le compte-rendu)
Experiment ==> Execute

Puis exécuter dans l'ordre les différents scripts de lancement (Dans l'ordre: deploy.sh ==> DNS_launch.sh)